using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FormView : MonoBehaviour
{
    public InputField NameField;
    [SerializeField] private InputField descriptionField;
    [SerializeField] private Dropdown raceDropdown;
    [SerializeField] private Dropdown classDropdown;
    [SerializeField] private Text statusBarText;

    public void UpdateFields()
    {
        List<string> playerInfo = new List<string>();

        string nick = NameField.text;
        string description = descriptionField.text;  
        string raceText = raceDropdown.options[raceDropdown.value].text;
        string classText = classDropdown.options[classDropdown.value].text;

        playerInfo.Add(nick);
        playerInfo.Add(description);
        playerInfo.Add(raceText);
        playerInfo.Add(classText);

        foreach (var playerInfoType in playerInfo)
        {
            Debug.Log(playerInfoType);
        }

        statusBarText.text = "";
    }

    public void EmptyWarning()
    {
        statusBarText.text = "Name field is empty!";
    }
}
