using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private GameObject form;
    [SerializeField] private FormView formView;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!form.active)
            {
                form.SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (formView.NameField.text != "")
            {
                formView.UpdateFields();
                form.SetActive(false);
            }
            else
            {
                formView.EmptyWarning();
            }
        }
    }
}
